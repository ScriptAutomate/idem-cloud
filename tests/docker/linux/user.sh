#!/bin/bash
set -e

printf "\n\033[0;44m---> Creating SSH user idem_cloud_user .\033[0m\n"

useradd -m -d /home/idem_cloud_user -G ssh idem_cloud_user -s /bin/bash -p ""
echo 'PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin"' >> /home/idem_cloud_user/.profile

# shellcheck disable=SC2129
echo "idem_cloud_user ALL=NOPASSWD:ALL" >> /etc/sudoers

# Give the new user ownership of the relevant test directories
chown -R idem_cloud_user:idem_cloud_user /srv

addgroup sftp

exec "$@"
