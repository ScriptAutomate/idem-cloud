def pytest_addoption(parser):
    group = parser.getgroup("Idem Cloud")
    group.addoption(
        "--docker-service",
        default="linux",
        help="Specify the docker service to use from tests/docker/docker-compose.yml",
    )
