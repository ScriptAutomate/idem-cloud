import mock
import os
import pytest
import socket


@pytest.fixture(scope="module")
def hub(hub):
    hub.pop.sub.add("idem.idem")

    with mock.patch(
        "sys.argv", ["idem"],
    ):
        hub.pop.config.load(["idem"], "idem", parse_cli=True)

    yield hub


def port_is_open(ip_address: str, port: int) -> bool:
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    if sock.connect_ex((ip_address, port)):
        # Port is not available if there was a return code
        return False
    else:
        # Port is Open
        return True


@pytest.fixture(scope="session")
def docker_compose_file(pytestconfig, request):
    # Get the docker service to use from the command line
    docker_service = request.config.getoption("docker_service")
    return os.path.join(
        pytestconfig.rootdir, "tests", "docker", docker_service, "docker-compose.yml"
    )


@pytest.fixture(scope="module")
def docker_ssh_port(docker_ip, docker_services, request) -> int:
    # Get the docker service to use from the command line
    docker_service = request.config.getoption("docker_service")

    # Find out which port ssh was mapped to
    docker_ssh_port = docker_services.port_for(docker_service, 22)

    # Wait until the SSH port is open
    docker_services.wait_until_responsive(
        timeout=10, pause=0.5, check=lambda: port_is_open(docker_ip, docker_ssh_port)
    )
    yield docker_ssh_port


@pytest.fixture(scope="session")
def docker_compose_project_name():
    # Use the same name every time so that unused images don't pile up
    return "idem_cloud_test"
