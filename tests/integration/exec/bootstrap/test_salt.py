import pytest


@pytest.mark.asyncio
async def test_create(hub, docker_ip: str, docker_ssh_port: int):
    assert await hub.exec.bootstrap.salt.create(
        "idem_cloud_user",
        docker_ip,
        tunnel_opts={"port": docker_ssh_port, "sudo": False},
        tunnel_plugin="asyncssh",
        version="",
    )
