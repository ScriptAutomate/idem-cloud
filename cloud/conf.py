import os
import tempfile

CLI_CONFIG = {}
CONFIG = {
    "heist_artifact_dir": {
        "type": str,
        "default": os.path.join(
            tempfile.gettempdir(), "idem-cloud-bootstrap-artifacts"
        ),
        "dyne": "idem",
        "os": "HEIST_ARTIFACT_DIR",
        "help": "The location of the local artifacts directory",
    },
    "salt_key_plugin": {
        "type": str,
        "default": "local",
        "dyne": "idem",
        "help": "The plugin to use to interract with a bootstrapped minion's salt key",
        "choices": ["api", "local", "raas"],
    },
    "heist_deploy_dir": {
        "type": str,
        "default": "/srv/heist/salt",
        "dyne": "idem",
        "os": "HEIST_DEPLOY_DIR",
        "help": "The place where a deployed binary will reside on the target",
    },
}
GLOBAL = {}
SUBS = {}
DYNE = {"states": ["states"], "exec": ["exec"]}
