from typing import Any, Dict


async def sig_create(
    hub, user: str, host: str, tunnel_opts: Dict[str, Any], tunnel_plugin: str, **kwargs
):
    ...


async def sig_get(hub) -> Dict[str, Any]:
    ...


async def sig_delete(hub) -> bool:
    ...


async def sig_update(hub) -> bool:
    ...
