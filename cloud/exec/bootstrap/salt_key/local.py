# Use a local Salt master's keys to accept a minion key
import os
import salt.config
import salt.key
import salt.syspaths


def __init__(hub):
    hub.exec.bootstrap.salt_key.local.DEFAULT_MINION_CONFIG = os.path.join(
        salt.syspaths.CONFIG_DIR, "minion"
    )


def accept_minion(hub, minion: str) -> bool:
    opts = salt.config.minion_config(
        hub.exec.bootstrap.salt_key.local.DEFAULT_MINION_CONFIG
    )
    salt_key = salt.key.Key(opts, io_loop=hub.pop.Loop)

    if minion not in salt_key.list_status("all"):
        return False

    salt_key.accept(
        match_dict={minion: minion}, include_denied=False, include_rejected=False
    )

    return minion in salt_key.list_status("accepted")


def delete_minion(hub, minion: str) -> bool:
    opts = salt.config.minion_config(
        hub.exec.bootstrap.salt_key.local.DEFAULT_MINION_CONFIG
    )
    salt_key = salt.key.Key(opts, io_loop=hub.pop.Loop)

    if minion not in salt_key.list_status("all"):
        hub.log.debug(f"The minion `{minion}` is already denied")
        return True

    salt_key.delete_key(
        match_dict={minion: minion}, preserve_minions=None, revoke_auth=False
    )

    return minion not in salt_key.list_status("all")
