import aiodns
import os
from typing import Any, Dict


def __init__(hub):
    hub.pop.sub.add(dyne_name="heist")
    hub.pop.loop.create()
    hub.exec.DNS = aiodns.DNSResolver(loop=hub.pop.Loop)


async def create(
    hub,
    user: str,
    host: str,
    tunnel_opts: Dict[str, Any] = None,
    tunnel_plugin: str = "asyncssh",
    **kwargs,
) -> bool:
    """
    Deploy a minion on the remote system and accept it's keys on the configured master
    """
    minion_id = kwargs.get("minion_id")

    if not minion_id:
        addr_info = await hub.exec.DNS.gethostbyaddr(host)
        minion_id = addr_info.name

    # Unique target name
    target_name = f"{user}@{host}({minion_id})"

    # Target configuration
    target = tunnel_opts or {}
    target["username"] = user
    target["host"] = host

    # Add target to tunnel connections
    assert await getattr(hub.tunnel, tunnel_plugin).create(
        name=target_name, target=target
    ), f"Could not connect to {user}@{host} with {tunnel_opts}"

    # Share this connection with heist
    hub.heist.CONS[target_name] = getattr(hub.tunnel, tunnel_plugin).CONS[target_name]

    # Add target to the roster
    hub.heist.ROSTERS[target_name] = {
        "artifact": "salt",
        # TODO should this be true or false?
        "bootstrap": True,
        "tunnel": tunnel_plugin,
        "user": user,
        "host": host,
    }

    # Create a directory for downloading artifacts
    os.makedirs(hub.OPT.idem.heist_artifact_dir, exist_ok=True)

    # Download the right salt artifact locally
    # TODO Use "detect_os" (or grainsv2:kernel) to determine exactly which binary/exe I need
    salt_bin = await hub.artifact.salt.get_artifact(
        t_name=target_name,
        t_type=tunnel_plugin,
        art_dir=hub.OPT.idem.heist_artifact_dir,
        # TODO get this from grainsv2
        t_os="linux",
        # Get the named salt version, default to latest from PyPi (empty string)
        ver=kwargs.get("version", ""),
    )

    # TODO make sure the deployed config has the right master
    remote_salt_bin = await hub.heist.salt_master.deploy(
        t_name=target_name,
        t_type=tunnel_plugin,
        bin_=salt_bin,
        run_dir=hub.OPT.idem.heist_deploy_dir,
    )
    remote_config_dir = os.path.join(hub.OPT.idem.heist_deploy_dir, "config")

    # TODO make sure the new minion service is running and enabled and such, with like sysctl and such
    assert await getattr(hub.tunnel, tunnel_plugin).cmd(
        name=target_name,
        command=f"{remote_salt_bin} minion --daemon -c {remote_config_dir} --user {user}",
        check=False,
        timeout=None,
    ), f"Could not connect to {user}@{host} with {tunnel_opts}"

    # Accept a key with the minion ID that I just created
    return getattr(
        hub.exec.bootstrap.salt_key, hub.OPT.idem.salt_key_plugin
    ).accept_minion(minion=minion_id)


async def get(hub):
    """
    Return information about the deployed minion
    """


async def delete(hub):
    """
    Remove this minion and reject it's keys
    """


async def update(hub):
    """
    Deploy the latest minion to the system
    """
    hub.heist.salt_master.update()
